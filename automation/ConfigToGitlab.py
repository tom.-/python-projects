#Simple script to regularly backup my config files.

import os
from pathlib import Path
import sh

home = os.environ["HOME"]
path = Path(os.environ["HOME"] + "/configs")

#Create configs dir if it doesn't exist
if not os.path.isdir(path):
    os.mkdir(path)

os.chdir(path)

filesToBackup = [home + "/.vimrc", "/etc/vimrc", "/etc/bashrc", home + "/.bashrc", "/etc/ansible/ansible.cfg"]

#Copy config files to dir
for file in filesToBackup:
    sh.cp("-r", file, path)

def gitPush():
    sh.git("add", ".")
    sh.git("commit", "-m", "Automated commit")
    sh.git("push")

gitPush()
