# Script which watches the Downloads folder and sorts newly-downloaded files into the appropriate directory.

import os
import time
from watchdog.observers import Observer
from watchdog.events import LoggingEventHandler
import shutil

if __name__ == "__main__":
    
    path = os.environ["HOME"] + "/Downloads"
    home = os.environ["HOME"]

    class Handler(LoggingEventHandler):
        def on_modified(self, event):
        #Move files to directory according to filetype
            for files in os.listdir(path):
                ext = files.split(".")[-1]
                if ext == "rpm":
                    shutil.move(path + "/" + files, home + "/rpms")
                elif ext == "zip" or ext == "tar" or ext == "gz":
                    shutil.move(path + "/" + files, home + "/zips")
                elif ext == "jpeg" or ext == "jpg" or ext == "gif" or ext == "png":
                    shutil.move(path + "/" + files, home + "/Pictures")
                elif ext == "iso":
                    shutil.move(path + "/" + files, home + "/isos")
                elif ext == "txt" or ext == "doc" or ext == "docx" or ext == "pdf":
                    shutil.move(path + "/" + files, home + "/Documents")
                else:
                    shutil.move(path + "/" + files, home + "/Uncategorised")

    observer = Observer()
    event_handler = Handler()
    observer.schedule(event_handler, path, recursive=True)
    observer.start()

    try:
        while True:
            time.sleep(10)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()
