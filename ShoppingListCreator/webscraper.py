import requests
from bs4 import BeautifulSoup as bs
import lxml
import pyinputplus as pyip

#Declare variables
recipes = []
ingredientslist = []

def gatherSearchTerms():
    print("Please enter your search terms, separated by spaces:")
    searchterms = pyip.inputStr(blockRegexes=[r"\d"]).replace(" ", "+")
    return searchterms

#searchterms = gatherSearchTerms()

def gatherRecipes(searchterms):
    # Gather 2 pages of results
    pageno = 2
    while pageno <= 2:
        # Form URL from searchterms and pageno
        searchurl = "https://www.bbcgoodfood.com/search/recipes/page/" + str(pageno) + "/?q=" + searchterms + "/&sort=-relevance"

        #Scraping search results
        searchpage = requests.get(searchurl).text
        searchsoup = bs(searchpage,"lxml")
        results = searchsoup.find_all("div", class_="template-search-universal__card")
    
        for result in results:
            title = result.find("a", class_="standard-card-new__article-title qa-card-link").text
            recipeurl = "https://bbcgoodfood.com" + result.find("a", class_="standard-card-new__article-title").attrs["href"]
            recipepage = requests.get(recipeurl).text
            recipesoup = bs(recipepage,"lxml")

            rating = result.find("span", class_="sr-only").text
            calories = recipesoup.find("td", class_="key-value-blocks__value").text
            ingredientstable = recipesoup.find_all("li", class_="pb-xxs pt-xxs list-item list-item--separator")
            for ingredient in ingredientstable:
                ingredientslist.append(ingredient.text)
            difficulty = recipesoup.find_all("div", class_="icon-with-text__children")[-2].text
            servesnumber = recipesoup.find_all("div", class_="icon-with-text__children")[-1].text
            try:
                preptime = str(recipesoup.find_all("time")[0].text) 
                cooktime = str(recipesoup.find_all("time")[1].text)
            except:
                preptime = str(recipesoup.find_all("time")[0].text)
                cooktime = 0

            #Not all recipes have a "Healthy" value. If "Healthy" value not present, treat as "not healthy".
            try:
                healthyvalue = recipesoup.find("span", class_="terms-icons-list__text d-flex align-items-center").text
                if healthyvalue:
                    healthy = True
                else:
                    healthy = False
            except:
                healthy = False

            #Debug:
            #print(title + "\n" + rating + "\n" + calories + "\n" + str(healthy))

            # Ignore recipes with ratings lower than 4.5

            if float(rating.split()[0]) > 4.5:
                recipes.append({
                                "Title" : title,
                                "Rating" : rating,
                                "Calories" : calories,
                                "Healthy" : healthy,
                                "Ingredients" : ingredientslist,
                                "Serves" : servesnumber,
                                "Preparation Time" : preptime,
                                "Cooking Time" : cooktime,
                                "Difficulty" : difficulty
                            })       
        pageno += 1
    return recipes

if __name__ == "__main__":
    gatherRecipes(gatherSearchTerms())
