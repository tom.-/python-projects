#This program will take scraped data from the webscraper.py program as an input and construct a shopping list from that data.

'''
Functionality:
1. Choose how many days to generate shopping list for (assume 1 day = 1 recipe) DONE
2. Choose to randomly generate suggestions or enter search terms and choose. DONE
3. Set recipe filters - difficulty, cooking time IN PROGRESS
4. Save accessed recipe pages to database, load from database if recipe has been saved before.
5. Add additional items to shopping list unrelated to recipes
6. "Email me shopping list" functionality ==> web frontend module
7. Price estimate

Web frontend will present buttons whcih will call the functions in this module.
'''

import random
import webscraper
import pyinputplus as pyip

class ShoppingList():
    def __init__(self,label):
        self.contents = []
        self.number_of_items = len(self.contents)
        self.label = label

    def __str__(self):
        printedlist = ""
        for i in self.contents:
            printedlist += printedlist + i + "\n"
        return printedlist

    def addItems(items):
        for i in items:
            self.contents.append(i)

difficulty_translate = { "Easy" : 1, "A challenge" : 2, "More effort" : 3 }

def numberOfDays():
    # Prompt for user input for how many days a shopping list should be generated for
    print("How many days do you want your shopping list to cover?")
    return pyip.inputInt()

def setDifficultyFilter():
    print("Please select a maximum recipe difficulty level:\n1 = Easy\n2 = A challenge\n3 = More effort")
    return pyip.inputChoice(["1","2","3"])

def randomRecipe(numberOfDays, difficulty_filter=3, cooktime_filter=60):
    baseingredients = ("pork", "beef", "lamb", "chicken", "vegetable", "fish", "prawn", "salmon")
    while True:
        randomrecipe = random.choice(webscraper.gatherRecipes(random.choice(baseingredients)))
        randomrecipes = [randomrecipe for i in range(numberOfDays) if difficulty_translate[randomrecipe["Difficulty"]] <= difficulty_filter and (randomrecipe["Preparation Time"] + randomrecipe["Cooking Time"]) <= cooktime_filter]
        print("Random recipes: ")
        for r in randomrecipes:
            print(r + "\n")
        print("Run again? Y/N: ")
        if pyip.inputYesNo():
            break   
    #print(randomrecipes)
    return randomrecipes

def addCustomItem():
    print("What would you like to add to the shopping list?: ")
    pyip.inputStr()

#ToDo - add database functionality. Store looked up recipes in database IF they don't already exist in there. 

            






